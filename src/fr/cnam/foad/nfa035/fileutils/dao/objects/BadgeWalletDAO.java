package fr.cnam.foad.nfa035.fileutils.dao.objects;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;

public class BadgeWalletDAO {
	
	private String path;
	
	/**
	 * 
	 * @param path
	 */
	public BadgeWalletDAO(String path) {
		this.path = path;
	}
	
	/**
	 * Ecrit un badge dans le fichier"
	 * @param image
	 * @throws IOException
	 */
	public void addBadge(File image) throws IOException{
        ImageByteArrayFrame media = new ImageByteArrayFrame(new ByteArrayOutputStream());
        // Sérialisation
        ImageSerializer serializer = new ImageSerializerBase64Impl();
        String encodedImage = (String) serializer.serialize(image);        
        
        try {
            FileWriter writer = new FileWriter(this.path);
            writer.write(encodedImage);
            writer.close();
          } catch (IOException e) {
            System.out.println("Une erreur s'est produite.");
          }
	}

	/**
	 * retourne le badge du fichier
	 * @param memoryBadgeStream
	 */
	public void getBadge(OutputStream memoryBadgeStream) {
		try{
		    String binaryFile = Files.readString(Paths.get(this.path)); 
		    ImageSerializer serializer = new ImageSerializerBase64Impl();
		    //suppression des sauts de ligne
		    byte[] decodedImage = (byte[]) serializer.deserialize(binaryFile.replaceAll("\n", "") );        
			memoryBadgeStream.write(decodedImage);
		}catch(Exception e){
			System.out.println("Erreur:" + e.getMessage());
		}
	}
}
