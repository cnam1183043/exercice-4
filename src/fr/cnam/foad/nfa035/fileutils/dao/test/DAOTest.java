package fr.cnam.foad.nfa035.fileutils.dao.test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.OutputStream;

import fr.cnam.foad.nfa035.fileutils.dao.objects.BadgeWalletDAO;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.fileutils.streaming.test.StreamingTest;

public class DAOTest {
	private static String RESOURCES_PATH = System.getProperty("user.dir") + "/";
	private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.csv");
	private static final Logger LOG = LogManager.getLogger(StreamingTest.class);

	@BeforeEach
	public void init() throws IOException {
	    if (walletDatabase.exists()){
	        walletDatabase.delete();
	        walletDatabase.createNewFile();
	    }
	}
	
	 @Test
	    public void testAddBadge(){
	        try {
	            File image = new File(RESOURCES_PATH + "petite_image.png");
	            BadgeWalletDAO dao = new BadgeWalletDAO(RESOURCES_PATH + "wallet.csv");
	            dao.addBadge(image);

	            String serializedImage = new String(Files.readAllBytes(walletDatabase.toPath()));
	            LOG.info("Le badge-wallet contient à présent cette image sérialisée:\n{}", serializedImage);

	            // Utilisation des outils pour comparer avec le résultat attendu
	            ImageSerializer serializer = new ImageSerializerBase64Impl();
	            String encodedImage = (String) serializer.serialize(image);

	            // astuce pour ignorer les différences de formatage entre outils de sérialisation base64:
	            serializedImage = serializedImage.replaceAll("\n","").replaceAll("\r","") ;

	            assertEquals(serializedImage, encodedImage);

	        } catch (Exception e) {
	            LOG.error("Test en échec ! ",e);
	            fail();
	        }
	    }
	 
	 @Test
	    public void testGetBadge(){
	        try {
	            BadgeWalletDAO dao = new BadgeWalletDAO(RESOURCES_PATH + "wallet_full.csv");
	            File extractedImage = new File(RESOURCES_PATH + "petite_image_extraite.png");

	            ByteArrayOutputStream memoryBadgeStream = new ByteArrayOutputStream();
	            dao.getBadge(memoryBadgeStream);
	            byte[] deserializedImage = memoryBadgeStream.toByteArray();            

	            // Vérification 1
	            byte [] originImage = Files.readAllBytes(new File(RESOURCES_PATH + "petite_image.png").toPath());
	            assertArrayEquals(Arrays.copyOfRange(originImage,0,originImage.length-2), deserializedImage);

	            // Vérification 2
	            dao = new BadgeWalletDAO(RESOURCES_PATH + "wallet_full.csv");
	            OutputStream fileBadgeStream = new FileOutputStream(extractedImage);
	            dao.getBadge(fileBadgeStream);

	            deserializedImage = Files.readAllBytes(new File(RESOURCES_PATH + "petite_image_extraite.png").toPath());
	            assertArrayEquals(Arrays.copyOfRange(originImage,0,originImage.length-2), deserializedImage);

	        } catch (Exception e) {
	            LOG.error("Test en échec ! ",e);
	            fail();
	        }
	    }
}
